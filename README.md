# Seminar Computer Graphics And Geometry Processing

In this Seminar I wrote "Report of Parametrization Quantization with Free Boundaries for Trimmed Quad Meshing" based on the similarly named paper by  [Lyon et. al.](https://dl.acm.org/doi/pdf/10.1145/3306346.3323019?casa_token=KwcgOUfdwnUAAAAA:1PjtbDRr2I-7qB3KC9WQ2hmVaJVTlkCOgHUhlHboCqNRgISFV22Yvmuv3pN0swxhhl22w5XlXhX8)

## Task
The focus of our task was to understand the assigned paper as well as predecessors and similar approaches of the presented methods.
The target audience are people with a basic understanding of computer science. Although the topic is advanced all relevant concepts are introduced.  
Questions as well as feedback are highly appreciated!

## Overview
In this report we dive into the world of quad meshing where we try to compute a quad mesh from a given triangle mesh. The edges in a triangle mesh cannot provide more than a topological meaning. In a quadmesh however we can align them with anisotropic properties like curvature which is useful for further processing.  
The paper original paper expands on a state-of-the-art algorithm called Quantized Global Parametrization that obtains a mostly regular mesh with relatively few singularities where edges have a geometric meaning.  
To obtain this mesh the surface is first parametrisized in a way that allows for an easy extraction of the quad mesh. This shifts the difficulty to the computation of an integral parametrization.  
The expansion on QGP removes the constraint of boundary alignment for inputs which contain boundaries (i.e. are not closed). The result as a more regular mesh often with fewer singularities.

## Content
I publish my report on Lyons et. al. work and the slides in which i presented my findings. The presentation had a 20 minute time constraint. 
